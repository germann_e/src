#ifndef OPAL_MATRIX_HH
#define OPAL_MATRIX_HH

#include <boost/numeric/ublas/matrix.hpp>

typedef boost::numeric::ublas::matrix<double> matrix_t;

#endif